﻿using System;
using System.Windows.Forms;

namespace IPZ
{
    public partial class AvtoLogForm : Form
    {
        public AvtoLogForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void AvtoLogForm_Load(object sender, EventArgs e)
        {

        }

        private void Worker_Click(object sender, EventArgs e)
        {
            AvtoLogForm.ActiveForm.Hide();
            Avtorization MyAvtorization = new Avtorization();
            MyAvtorization.ShowDialog();
            Close();
        }

        private void Client_Click(object sender, EventArgs e)
        {
            AvtoLogForm.ActiveForm.Hide();
            Loginization MyLoginization = new Loginization();
            MyLoginization.ShowDialog();
            Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
