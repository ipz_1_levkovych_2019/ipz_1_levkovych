﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace IPZ
{
    class Connect
    {
        private const string host = "127.0.0.1";
        private const int port = 8888;
        static private TcpClient client;
        static private NetworkStream stream;

        static private bool Connection()
        {
            client = new TcpClient();
            try
            {
                client.Connect(host, port); //подключение клиента
                stream = client.GetStream(); // получаем поток
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        static internal string SendMessage(string message)
        {
            if (Connect.Connection())
            {
                Thread.Sleep(10);
                byte[] data = Encoding.Unicode.GetBytes(message);
                stream.Write(data, 0, data.Length);
                string answer = ReceiveMessage();
                Disconnect();
                if (answer != "bad:(")
                {
                    return answer;
                }
                else
                {
                    return "bad:(";
                }
            }
            else
            {
                MessageBox.Show("Відсутій зв'язок з сервером");
                return null;
            }
        }
        static internal string SendMessageNRec(string message)
        {
            if (Connect.Connection())
            {
                Thread.Sleep(10);
                byte[] data = Encoding.Unicode.GetBytes(message);
                stream.Write(data, 0, data.Length);
                string answer = ReceiveMessage();
                Disconnect();
                return answer;
            }
            else
            {
                MessageBox.Show("Відсутій зв'язок з сервером");
                return null;
            }
        }
        static private string ReceiveMessage()
        {
            try
            {
                byte[] data = new byte[64];
                StringBuilder builder = new StringBuilder();
                int bytes = 0;
                do
                {
                    bytes = stream.Read(data, 0, data.Length);
                    builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                }
                while (stream.DataAvailable);
                string message = builder.ToString();
                return message;
            }
            catch
            {
                MessageBox.Show("Підключення перерване");
                Disconnect();
                return "Connection lost";
            }
        }
        static private void Disconnect()
        {
            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
        }
    }
}