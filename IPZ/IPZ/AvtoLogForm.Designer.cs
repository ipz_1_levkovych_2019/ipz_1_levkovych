﻿namespace IPZ
{
    partial class AvtoLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AvtoLogForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.Client = new System.Windows.Forms.Button();
            this.Worker = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.Client);
            this.panel1.Controls.Add(this.Worker);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(750, 385);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Client
            // 
            this.Client.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Client.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Client.Font = new System.Drawing.Font("Times New Roman", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Client.Location = new System.Drawing.Point(0, 0);
            this.Client.Name = "Client";
            this.Client.Size = new System.Drawing.Size(750, 115);
            this.Client.TabIndex = 0;
            this.Client.Text = "Ввійти від імені клієнта";
            this.Client.UseVisualStyleBackColor = false;
            this.Client.Click += new System.EventHandler(this.Client_Click);
            // 
            // Worker
            // 
            this.Worker.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Worker.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Worker.Font = new System.Drawing.Font("Times New Roman", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Worker.Location = new System.Drawing.Point(0, 270);
            this.Worker.Name = "Worker";
            this.Worker.Size = new System.Drawing.Size(750, 115);
            this.Worker.TabIndex = 1;
            this.Worker.Text = "Ввійти від імені працівника";
            this.Worker.UseVisualStyleBackColor = false;
            this.Worker.Click += new System.EventHandler(this.Worker_Click);
            // 
            // AvtoLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 385);
            this.Controls.Add(this.panel1);
            this.Name = "AvtoLogForm";
            this.Text = "Choose your fighter";
            this.Load += new System.EventHandler(this.AvtoLogForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Client;
        private System.Windows.Forms.Button Worker;
    }
}