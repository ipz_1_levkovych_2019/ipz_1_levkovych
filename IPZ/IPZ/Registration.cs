﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace IPZ
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
            this.TextBox3.AutoSize = false;
            this.TextBox3.Size = new Size(this.TextBox3.Size.Width, 72);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Registration_Load(object sender, EventArgs e)
        {

        }

        private void Inbutton_Click(object sender, EventArgs e)
        {
            string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            if (string.IsNullOrEmpty(TextBox1.Text))
            {
                MessageBox.Show("Введіть пошту");
                return;
            }

            if (!Regex.IsMatch(TextBox1.Text, pattern))
            {
                MessageBox.Show("Формат пошти не є валідним");
                return;
            }

            if (!(TextBox1.Text.Length >= 8 && TextBox1.Text.Length <= 30))
            {
                MessageBox.Show("Пошта повинна містити мінімум 8 символів та максимум 30!");
                return;
            }

            if (string.IsNullOrEmpty(TextBox2.Text))
            {
                MessageBox.Show("Введіть логін");
                return;
            }

            if (!(TextBox2.Text.Length >= 4 && TextBox2.Text.Length <= 20))
            {
                MessageBox.Show("Логін повинен містити мінімум 4 символи та максимум 30!");
                return;
            }

            if (string.IsNullOrEmpty(TextBox3.Text))
            {
                MessageBox.Show("Введіть пароль");
                return;
            }
            if (!(TextBox3.Text.Length >= 8))
            {
                MessageBox.Show("Пароль повинен містити мінімум 8 символів!");
                return;
            }

            else
            {
                string answer = Connect.SendMessage(String.Format("AddClient-_-{0}-_-{1}-_-{2}", TextBox1.Text, TextBox2.Text, TextBox3.Text));
                if (answer != null)
                {
                    MessageBox.Show("Зареєстровано!");

                    Hide();
                    Loginization MyLoginization = new Loginization();
                    MyLoginization.ShowDialog();
                    Close();

                }
                else
                {
                    MessageBox.Show("Відмовлено в реєстрації!");
                }
            }
        }

        private void emailTextBox_Validating(object sender, CancelEventArgs e)
        {

        }

        private void loginTextBox_Validating(object sender, CancelEventArgs e)
        {

        }

        private void passwordTextBox_Validating(object sender, CancelEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
