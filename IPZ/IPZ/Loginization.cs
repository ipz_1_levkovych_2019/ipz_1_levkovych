﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace IPZ
{
    public partial class Loginization : Form
    {


        public Loginization()
        {
            InitializeComponent();
            this.textBox2.AutoSize = false;
            this.textBox2.Size = new Size(this.textBox2.Size.Width, 72);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Registerlabel_MouseEnter(object sender, EventArgs e)
        {
            Registerlabel.ForeColor = Color.Blue;
        }

        private void Registerlabel_MouseLeave(object sender, EventArgs e)
        {
            Registerlabel.ForeColor = Color.Black;
        }

        private void Loginization_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Inbutton_Click(object sender, EventArgs e)
        {
            string login = textBox1.Text;
            string password = textBox2.Text;

            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show("Введіть логін");
                return;
            }
            if (!(textBox1.Text.Length >= 4 && textBox1.Text.Length <= 20))
            {
                MessageBox.Show("Логін повинен містити мінімум 4 символи та максимум 30!");
                return;
            }
            else if (string.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("Введіть пароль");
                return;
            }
            if (!(textBox2.Text.Length >= 8))
            {
                MessageBox.Show("Пароль повинен містити мінімум 8 символів!");
                return;
            }
            else
            {
                string answer = Connect.SendMessage(String.Format("Check:ClientLog&Pass-_-{0}-_-{1}", login, password));
                if (answer != "bad:(" )
                {
                        Loginization.ActiveForm.Hide();
                        Serviceforclient MyServiceforclient = new Serviceforclient(Convert.ToInt32(answer.Split('`')[1]));
                        MyServiceforclient.ShowDialog();
                        Close();
                }
                else
                {
                    MessageBox.Show("Такого користувача не існує!");
                }
            }
        }

        private void Registerlabel_Click(object sender, EventArgs e)
        {
            Hide();
            Registration MyRegistration = new Registration();
            MyRegistration.ShowDialog();
            Close();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
