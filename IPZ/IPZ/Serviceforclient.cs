﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPZ
{

    public partial class Serviceforclient : Form
    {

        List<bool> checkBoxes = new List<bool>();
        List<string> ServiceName = new List<string>();
        List<string> ServiceDate = new List<string>();

        int CId;
        public Serviceforclient(int CId)
        {
            this.CId = CId;
            InitializeComponent();

            Inbutton.Enabled = false;
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            comboBox3.Enabled = false;
            comboBox4.Enabled = false;
            comboBox5.Enabled = false;
            comboBox6.Enabled = false;

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void listBox11_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Inbutton_Click(object sender, EventArgs e)
        {
            List<ComboBox> comboBoxes = new List<ComboBox>(new ComboBox[] { comboBox1, comboBox2, comboBox3, comboBox4, comboBox5, comboBox6 });
            ServiceName.AddRange(new string[] { "Стрижка", "Фарбування волосся", "Манікюр", "Педикюр", "Нарощування вій", "Макіяж" });
            checkBoxes.AddRange(new bool[] { checkBox1.Checked, checkBox2.Checked, checkBox3.Checked, checkBox4.Checked, checkBox5.Checked, checkBox6.Checked });
            StringBuilder builder = new StringBuilder();
            if (!checkBoxes.Contains(true))
            {
                MessageBox.Show("Оберіть щось!");
                return;
            }
            for (int i = 0; i < 6; i++)
            {
                if (checkBoxes[i])
                {
                    try
                    {
                        if (comboBoxes[i].SelectedItem.ToString().Length > 5)
                        {
                            builder.Append(i.ToString() + '`' + ServiceName[i] + '`' + CId.ToString() + '`' + comboBoxes[i].SelectedItem.ToString() + '~');
                            
                        }
                        else
                        {
                            MessageBox.Show(String.Format("Ви не обрали час послуги"));
                            return;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(String.Format("Ви не обрали час послуги"));
                        return;
                        throw;
                    }
                }
            }

            string answer = Connect.SendMessage(String.Format("MakeOrder-_-{0}", builder.ToString().TrimEnd('~')));
            if (answer == "Added")
            {
                string message = $"Ви успішно замовили послугу";
                MessageBox.Show(message);
                // Шото сделать
                Hide();
                Serviceforclient newF = new Serviceforclient(CId);
                newF.ShowDialog();
                Close();
            }
            

        }

        private void listBox22_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Inbutton.Enabled = (checkBox1.CheckState == CheckState.Checked);
            comboBox1.Enabled = (checkBox1.CheckState == CheckState.Checked);

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Inbutton.Enabled = (checkBox2.CheckState == CheckState.Checked);
            comboBox2.Enabled = (checkBox2.CheckState == CheckState.Checked);
        }

        private void checkBox3_CheckedChanged_1(object sender, EventArgs e)
        {
            Inbutton.Enabled = (checkBox3.CheckState == CheckState.Checked);
            comboBox3.Enabled = (checkBox3.CheckState == CheckState.Checked);
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            Inbutton.Enabled = (checkBox4.CheckState == CheckState.Checked);
            comboBox4.Enabled = (checkBox4.CheckState == CheckState.Checked);
        }

        private void checkBox5_CheckedChanged_1(object sender, EventArgs e)
        {
            Inbutton.Enabled = (checkBox5.CheckState == CheckState.Checked);
            comboBox5.Enabled = (checkBox5.CheckState == CheckState.Checked);
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            Inbutton.Enabled = (checkBox6.CheckState == CheckState.Checked);
            comboBox6.Enabled = (checkBox6.CheckState == CheckState.Checked);
        }

        private void Serviceforclient_Load(object sender, EventArgs e)
        {
            List<CheckBox> checkBoxesN = new List<CheckBox>();
            checkBoxesN.AddRange(new CheckBox[] { checkBox1, checkBox2, checkBox3, checkBox4, checkBox5, checkBox6 });
            List<ComboBox> comboBoxes = new List<ComboBox>(new ComboBox[] { comboBox1, comboBox2, comboBox3, comboBox4, comboBox5, comboBox6 });
            ServiceName.AddRange(new string[] { "Стрижка", "Фарбування волосся", "Манікюр", "Педикюр", "Нарощування вій", "Макіяж" });
            string answer = Connect.SendMessage("GetTimeForServices-_-");
            if (answer.Contains('`'))
            {
                string[] answerL = answer.Split('~');
                for (int i = 0; i < answerL.Length; i++)
                {
                    string[] elements = answerL[i].Split('#');

                    string[] dateEl = elements[1].Split('`');

                    comboBoxes[Convert.ToInt32(elements[0])].Items.AddRange(dateEl);
                    
                }
                for (int i = 0; i < 6; i++)
                {
                    if (comboBoxes[i].Items.Count == 0)
                    {
                        checkBoxesN[i].Enabled = false;
                    }
                }
                // обробка вхідних годин
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            AvtoLogForm MyAvtoLogForm = new AvtoLogForm();
            MyAvtoLogForm.ShowDialog();
            Close();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
