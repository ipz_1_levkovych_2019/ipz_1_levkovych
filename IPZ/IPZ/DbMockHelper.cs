﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ
{
    public static class DbMockHelper
    {
        public static Dictionary<Identity, string> ClientsClaims = new Dictionary<Identity, string>();

        public static Dictionary<string, string> WorkerClaims = new Dictionary<string, string>
        {
            { "boris", "pa$$word" },
        };
    }
}
