﻿using System;
using System.Collections.Generic;

namespace IPZ
{
    public partial class Client
    {
        public Client(int id, string email, string login, string password)
        {
            Id = id;
            Email = email;
            Login = login;
            Password = password;
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
