﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZ
{
    public class Identity
    {
        public string Email { get; set; }

        public string LoginName { get; set; }
    }
}
