﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPZ
{
    public partial class Serviceforworkers : Form
    {
        int WId;
        public Serviceforworkers(int WId)
        {
            this.WId = WId;
            InitializeComponent();
        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void Inbutton_Click(object sender, EventArgs e)
        {
            Hide();
            AvtoLogForm MyAvtoLogForm = new AvtoLogForm();
            MyAvtoLogForm.ShowDialog();
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Serviceforworkers_Load(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            string answer = Connect.SendMessage(String.Format("GetOrderList-_-" + WId));
            if (answer.Length != 0 && answer != "bad:(")
            {
                string[] patients = answer.Split('~');
                foreach (var item in patients)
                {
                    string[] param = item.Split('`');
                    dataGridView1.Rows.Add(param[0], param[1], param[2]);
                    
                }
            }
            else
            {
                MessageBox.Show("У вас немає записів!");
                Hide();
                AvtoLogForm MyAvtoLogForm = new AvtoLogForm();
                MyAvtoLogForm.ShowDialog();
                Close();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
