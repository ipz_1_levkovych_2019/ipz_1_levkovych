﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeautyShopServer
{
    class RequestAnalyze
    {
        static internal void Analyze(string command, ServerObject server, string id)
        {
            string answer;
            string[] commandL = command.Split("-_-");
            Console.WriteLine(command);
            switch (commandL[0])
            {
                case "Check:WorkerLog&Pass":
                    answer = DBHandle.CheckWorker(commandL[1], commandL[2]);
                    if (answer != null)
                    {
                        ClientObject.SuccesssLog(String.Format("Operation {0} successfully competed!", commandL[0]));
                        server.SendMessage(answer, id);
                    }
                    else
                    {
                            ClientObject.ErrorLog(String.Format("Uncorrect command: {0}", commandL[0]));
                            server.SendMessage(null, id);
                    }
                    break;
                case "Check:ClientLog&Pass":
                    answer = DBHandle.CheckClient(commandL[1], commandL[2]);
                    if (answer != null)
                    {
                        ClientObject.SuccesssLog(String.Format("Operation {0} successfully competed!", commandL[0]));
                        server.SendMessage(answer, id);
                    }
                    else
                    {
                        ClientObject.ErrorLog(String.Format("Uncorrect command: {0}", commandL[0]));
                        server.SendMessage("bad:(", id);
                    }
                    break;
                case "AddClient":
                    if (DBHandle.AddClient(commandL[1], commandL[2], commandL[3]))
                    {
                        ClientObject.SuccesssLog(String.Format("Operation {0} successfully competed!", commandL[0]));
                        server.SendMessage("Added", id);
                    }
                    else
                    {
                        ClientObject.ErrorLog(String.Format("Empty DB"));
                        server.SendMessage(null, id);
                    }
                    break;
                case "GetOrderList":
                    answer = DBHandle.GetOrderList(commandL[1]);
                    if (answer != null)
                    {
                        ClientObject.SuccesssLog(String.Format("Operation {0} successfully competed!", commandL[0]));
                        server.SendMessage(answer, id);
                    }
                    else
                    {
                        ClientObject.ErrorLog(String.Format("Empty DB"));
                        server.SendMessage(null, id);
                    }
                    break;
                case "GetTimeForServices":
                    answer = DBHandle.GetTimeForServices();
                    if (answer != null)
                    {
                        ClientObject.SuccesssLog(String.Format("Operation {0} successfully competed!", commandL[0]));
                        server.SendMessage(answer, id);
                    }
                    else
                    {
                        ClientObject.ErrorLog(String.Format("Empty DB"));
                        server.SendMessage(null, id);
                    }
                    break;
                case "MakeOrder":
                    if (DBHandle.MakeOrder(commandL[1]))
                    {
                        ClientObject.SuccesssLog(String.Format("Operation {0} successfully competed!", commandL[0]));
                        server.SendMessage("Added", id);
                    }
                    else
                    {
                        ClientObject.ErrorLog(String.Format("Empty DB"));
                        server.SendMessage(null, id);
                    }
                    break;
                    //case "AddOrder":
                    //    if (DBHandle.AddOrder(Convert.ToInt32(commandL[1]), Convert.ToInt32(commandL[2]), Convert.ToDateTime(commandL[3])))
                    //    {
                    //        ClientObject.SuccesssLog(String.Format("Operation {0} successfully competed!", commandL[0]));
                    //        server.SendMessage("Added", id);
                    //    }
                    //    else
                    //    {
                    //        ClientObject.ErrorLog(String.Format("Operation {0} successfully INcompeted!", commandL[0]));
                    //        server.SendMessage(null, id);
                    //    }
                    //    break;
                    //case "GetOrderTime":
                    //    answer = DBHandle.GetOrderTime(Convert.ToInt32(commandL[1]));
                    //    if (answer != null)
                    //    {
                    //        ClientObject.SuccesssLog(String.Format("Operation {0} successfully competed!", commandL[0]));
                    //        server.SendMessage(answer, id);
                    //    }
                    //    else
                    //    {
                    //        ClientObject.ErrorLog(String.Format("Empty DB"));
                    //        server.SendMessage(null, id);
                    //    }
                    //    break;
            }
        }
    }
}


