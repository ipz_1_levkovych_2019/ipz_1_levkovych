﻿using System;
using System.Net.Sockets;
using System.Text;

namespace BeautyShopServer
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        TcpClient client;
        ServerObject server; // объект сервера

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }
        // Scaffold-DbContext "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Programming\C#\Uras\Lab3\Проект\SecSysServer\SecSysDB.mdf;Integrated Security=True" Microsoft.EntityFrameworkCore.SqlServer

        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                // посылаем сообщение о входе в чат всем подключенным пользователям
                Console.WriteLine("User {0} connected to server", Id);
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        string message = GetMessage();
                        if (message == "")
                        {
                            ClientObject.ServerLog(String.Format("User left"));
                            break;
                        }
                        if (message.Contains("-_-"))
                        {
                            RequestAnalyze.Analyze(message, server, Id);
                        }

                    }
                    catch
                    {
                        ClientObject.ServerLog(String.Format("User left"));
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                server.RemoveConnection(this.Id);
                Close();
            }
        }

        private string GetMessage()
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);
            return builder.ToString();
        }
        static protected internal void ServerLog(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("{0}", new string('#', Console.WindowWidth));
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition((Console.WindowWidth - message.Length) / 2, Console.CursorTop);
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("{0}", new string('#', Console.WindowWidth));
            Console.ForegroundColor = ConsoleColor.White;
        }
        static protected internal void SuccesssLog(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
        static protected internal void ErrorLog(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
        static protected internal void ErrorServerLog(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0}", new string('-', Console.WindowWidth));
            Console.SetCursorPosition((Console.WindowWidth - message.Length) / 2, Console.CursorTop);
            Console.WriteLine(message);
            Console.WriteLine("{0}", new string('-', Console.WindowWidth));
            Console.ForegroundColor = ConsoleColor.White;
        }

        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
    }
}