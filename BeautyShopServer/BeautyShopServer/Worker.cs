﻿using System;
using System.Collections.Generic;

namespace BeautyShopServer
{
    public partial class Worker
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
    }
}
