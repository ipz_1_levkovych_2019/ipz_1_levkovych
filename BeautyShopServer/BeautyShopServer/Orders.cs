﻿using System;
using System.Collections.Generic;

namespace BeautyShopServer
{
    public partial class Orders
    {
        public Orders(int id, string serviceName, int? clientId, int? workerId, DateTime? date)
        {
            Id = id;
            ServiceName = serviceName;
            ClientId = clientId;
            WorkerId = workerId;
            Date = date;
        }

        public int Id { get; set; }
        public string ServiceName { get; set; }
        public int? ClientId { get; set; }
        public int? WorkerId { get; set; }
        public DateTime? Date { get; set; }
    }
}
