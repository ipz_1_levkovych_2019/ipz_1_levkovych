﻿using System;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Reflection;
using System.IO;

namespace BeautyShopServer
{
    class Program
    {
        static ServerObject server; // сервер
        static Thread listenThread; // потока для прослушивания
        
        static void Main(string[] args)
        {
            //DBHandle.GetTimeForServices();
            try
            {
                server = new ServerObject();
                listenThread = new Thread(new ThreadStart(server.Listen));
                listenThread.Start(); //старт потока
            }
            catch (Exception ex)
            {
                server.Disconnect();
                Console.WriteLine(ex.Message);
            }
        }
    }
}