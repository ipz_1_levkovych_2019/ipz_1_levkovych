﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BeautyShopServer

{
    class DBHandle
    {
        static internal string CheckWorker(string checkLogin, string checkPassword)
        {
            using (BeautyShopServer db = new BeautyShopServer())
            {
                var workers = db.Worker.ToList();
                foreach (Worker d in workers)
                {
                    if (checkLogin == d.Login && checkPassword == d.Password)
                    {
                        return "Worker`" + d.Id;
                    }
                }
            }
            return null;
        }
        static internal string CheckClient(string checkLogin, string checkPassword)
        {
            using (BeautyShopServer db = new BeautyShopServer())
            {
                var clients = db.Client.ToList();
                foreach (Client client in clients)
                {
                    if (checkLogin == client.Login && checkPassword == client.Password)
                    {
                        return "Client`" + client.Id;
                    }
                }
            }
            return null;
        }
        static internal string GetOrderList(string WorkerId)
        {
            using (BeautyShopServer db = new BeautyShopServer())
            {
                var fdL = db.Orders.Where(O => O.WorkerId == Convert.ToInt32(WorkerId)).ToList();
                


                if (fdL.Count != 0)
                {
                    StringBuilder builder = new StringBuilder();
                    foreach (Orders fd in fdL)
                    {
                       var ClientName = db.Client.Where(o => o.Id == fd.ClientId).ToList().First().Login.ToString();
                       builder.Append(fd.ServiceName + '`' + ClientName + '`' + fd.Date.ToString() + '~');
                    }
                    return builder.ToString().Trim('~');
                }
                else
                {
                    return "bad:(";
                }
            }
        }
        //static internal string GetDoctorFreeTime(int DoctorId)
        //{
        //    using (BeautyShopServer db = new BeautyShopServer())
        //    {
        //        List<DateTime?> workDateTimes = new List<DateTime?>();
        //        List<int> workDays = new List<int>();
        //        List<int> workHours = new List<int>();
        //        List<Orders> fdL;
        //        workHours.AddRange(new int[] { 12, 16, 18 });
        //        DateTime AppDay = DateTime.Today.AddDays(1);
        //        foreach (var item2 in workHours)
        //        {
        //            workDateTimes.Add(AppDay.AddHours(item2));
        //        }
        //        var EmptyWorkTime = db.Orders.Where(a => a.DoctorId == DoctorId).ToList();
        //        var selectedH = from t in EmptyWorkTime select t.Date;
        //        var res = workDateTimes.Except(selectedH);
        //        if (res != null)
        //        {
        //            StringBuilder builder = new StringBuilder();
        //            foreach (var fd in res)
        //            {
        //                builder.Append(fd.ToString() + '`');
        //            }
        //            return builder.ToString().Trim('`');
        //        }
        //        else
        //        {
        //            return "bad";
        //        }
        //    }
        //}
        static internal string GetTimeForServices()
        {

            using (BeautyShopServer db = new BeautyShopServer())
            {
                StringBuilder builder = new StringBuilder();
                DateTime AppDay = DateTime.Today.AddDays(1);
                var workers = db.Worker.ToList();
                foreach (var worker in workers)
                {
                    List<int> workHours = new List<int>();
                    workHours.AddRange(new int[] { 11, 12, 13, 14, 15, 16, 17, 18 });
                    List<int> workDays = new List<int>();
                    workDays.AddRange(new int[] { 0, 1, 2 });
                    List<DateTime?> workDateTimes = new List<DateTime?>();
                    foreach (var days in workDays)
                    {
                        foreach (var hours in workHours)
                        {
                            workDateTimes.Add(AppDay.AddDays(days).AddHours(hours));
                        }
                    }
                    workDateTimes.Sort();
                    var workerNotEmptyHours = db.Orders.Where(h => h.WorkerId == worker.Id).Select(h => h.Date).ToList();
                    var res = workDateTimes.Except(workerNotEmptyHours);
                    if (res.Count() != 0)
                    {
                        builder.Append(worker.Id.ToString() + '#');
                        foreach (var item in res)
                        {
                            builder.Append( item.ToString() + '`');
                        }
                        builder = new StringBuilder(builder.Append('~').ToString().TrimEnd('`'));
                        
                    }
                }
                return builder.ToString().TrimEnd('~');
            }
        }
        //static internal bool AddOrder(int id, string ServiceName, int ClientID, int WorkerID, DateTime dateTime)
        //{
        //    try
        //    {
        //        using (BeautyShopServer db = new BeautyShopServer())
        //        {
        //            int orderId;
        //            try
        //            {
        //                orderId = db.Orders.ToList().Last().Id + 1;
        //            }
        //            catch (Exception)
        //            {
        //                orderId = 0;
        //                throw;
        //            }

        //            Orders newOrder = new Orders(orderId, ServiceName, ClientID, WorkerID, dateTime);

        //            db.Orders.Add(newOrder);
        //            db.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        return false;
        //    }
        //}
        static internal bool AddClient(string Email, string Login, string Password)
        {
            try
            {
                using (BeautyShopServer db = new BeautyShopServer())
                {
                    int newId = db.Client.ToList().LastOrDefault().Id + 1;
                    Client newClient = new Client(newId, Email, Login, Password);

                    db.Client.Add(newClient);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        static internal bool MakeOrder(string input)
        {
            try
            {
                
                using (BeautyShopServer db = new BeautyShopServer())
                {
                    string[] inputE = input.Split('~');
                    int newId = db.Orders.ToList().LastOrDefault().Id + 1;
                    foreach (var item in inputE)
                    {
                        string[] inputEl = item.Split('`');
                        Orders newOrder = new Orders(newId, inputEl[1], Convert.ToInt32(inputEl[2]), Convert.ToInt32(inputEl[0]), Convert.ToDateTime(inputEl[3]));
                        db.Orders.Add(newOrder);
                        db.SaveChanges();
                        newId++;
                    }
                    
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        



    }
}
